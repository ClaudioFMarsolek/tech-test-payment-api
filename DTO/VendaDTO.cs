using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json.Serialization;
using System.Threading.Tasks;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.DTO
{
    public class VendaDTO
    {
        [JsonIgnore]
        public int Id { get; set; }
        [JsonIgnore]
        public Vendedor Vendedor { get; set; }
        public DateTime Data { get; set; }
        [JsonIgnore]
        public List<ItemDTO> Itens { get; set; }
        public EnumStatus StatusVenda { get; set; }  
        public int ItemId { get; set; }
        public int VendedorId { get; set; }
        public int VendaId { get; set; }
    }
}