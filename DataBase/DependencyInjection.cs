using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

namespace tech_test_payment_api.DataBase
{
    public static class DependencyInjection {
            public static IServiceCollection AddInfrastructure(this IServiceCollection services, IConfiguration configuration) {
                services.AddDbContext<AppDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("ConexaoPadrao"), b => b.MigrationsAssembly(typeof(AppDbContext).Assembly.FullName)));

                return services;
            }
        }

}