using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tech_test_payment_api.DataBase;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;
using tech_test_payment_api.Services;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
        public class VendedoresController : ControllerBase
        {
            private readonly VendedorService _vendedorService;
        

        public VendedoresController (VendedorService vendedorService){
            _vendedorService = vendedorService;
        }

        [HttpGet]
        public IActionResult Listar(){
            var list = _vendedorService.Listar();
            if(list.Count == 0){
                return NotFound("Nenhum Vendedor encontrado");
            }
            return Ok(list);
        }

        [HttpGet("{id}")]
        public IActionResult BuscarId(int id){
            var busca = _vendedorService.BuscarId(id);
            if(busca is null){
                return NotFound("Nenhum Vendedor Encontrado");
            }
            return Ok(busca);
        }

        [HttpPost]
        public IActionResult Cadastrar(VendedorDTO vendedorDTO){
            var cadastro = _vendedorService.Cadastrar(vendedorDTO);
            return Ok(vendedorDTO);
        }

        [HttpPut]
        public IActionResult Editar(VendedorDTO vendedorDTO){
            var edit = _vendedorService.Editar(vendedorDTO);
            return Ok(vendedorDTO);
        }

        [HttpDelete]
        public IActionResult Excluir(int id){
            var del = _vendedorService.Excluir(id);
            return Ok("Vendedor Excluído com sucesso!");
        }
    }
}
