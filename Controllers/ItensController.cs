using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;
using tech_test_payment_api.Services;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class ItensController : ControllerBase
    {
       private readonly ItemService _itemService;

       public ItensController(ItemService itemService){
            _itemService = itemService;
       }

       [HttpGet]
       public IActionResult Listar() {
            var listaItens = _itemService.Listar();
            if (listaItens.Count == 0)
                return NotFound("Lista de itens vazia");
            return Ok(listaItens);
       }

       [HttpGet("{id}")]
       public IActionResult BuscarId(int id) {
            var itemId = _itemService.BuscarId(id);
            if (itemId is null)
                return NotFound("Nenhum item encontrado");
            return Ok(itemId);
       }

       [HttpPost]
       public IActionResult Cadastrar(ItemDTO itemDTO) {
            var novoItem = _itemService.Cadastrar(itemDTO);
            return Ok(novoItem);
       }

       [HttpPut]
       public IActionResult Editar(ItemDTO itemDTO) {
            var itemEdit = _itemService.Editar(itemDTO);
            return Ok(itemEdit);
       }

       [HttpDelete("{id}")]
       public IActionResult Excluir(int id) {
            var itemDelete = _itemService.Excluir(id);
           return Ok("Item excluído!"); 
       }

    }
}