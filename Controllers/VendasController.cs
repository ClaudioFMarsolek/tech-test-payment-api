using Microsoft.AspNetCore.Mvc;
using tech_test_payment_api.DataBase;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;
using tech_test_payment_api.Services;

namespace tech_test_payment_api.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class VendasController : ControllerBase
    {
        private readonly VendaService _vendaService;
        public VendasController(VendaService vendaService){
            _vendaService = vendaService;
        }

        [HttpGet]
        public IActionResult Listar(){
            var listaVenda = _vendaService.Listar();
            if(listaVenda.Count == 0)
                return NotFound("Lista vazia");
            return Ok(listaVenda);
        }

        [HttpGet("{id}")]
        public IActionResult BuscarVendaId(int id){
            var buscaId = _vendaService.BuscarVendaId(id);
            if(buscaId is null)
                return NotFound("Lista Vazia");
            return Ok(buscaId);
        }

        [HttpGet("BuscarVendaPorStatus")]
        public IActionResult BuscarVendaStatus(EnumStatus status){
            var buscaPorStatus = _vendaService.BuscarVendaStatus(status);
            if(buscaPorStatus.Count == 0)
                return NotFound("Lista Vazia");
            return Ok(buscaPorStatus);
        }

        [HttpPost]
        public IActionResult CadastrarNovaVenda(VendaDTO vendaDTO, int id) {
            var novaVenda = _vendaService.CriarVenda(vendaDTO, id);
            return Ok(novaVenda);
        }

        [HttpPut]
        public IActionResult Editar(VendaDTO vendaDTO){
            var venda = _vendaService.Editar(vendaDTO);
            return Ok("Item editado com sucesso");
        }

        [HttpDelete("Excluir/{id}")]
        public IActionResult Excluir(int id){
            var venda = _vendaService.Excluir(id);
            if(venda is null)
                return NotFound("Nenhuma venda Encontrada");
            return Ok ("Venda excluida com sucesso");
        }

        [HttpPost("StatusAtualizado_RealizarPagamento")]
        public IActionResult RealizarPagamento(int id){
            var venda = _vendaService.RealizarPagamento(id);
            return Ok(venda);

        }
        [HttpPost("StatusAtualizado_EnviadoParaTransportadora")]
        public IActionResult EnviadoParaTransportadora(int id){
            var venda = _vendaService.EnviadoParaTransportadora(id);
            return Ok(venda);

        }
        [HttpPost("StatusAtualizado_PedidoEntregue")]
        public IActionResult PedidoEntregue(int id){
            var venda = _vendaService.PedidoEntregue(id);
            return Ok(venda);

        }
        [HttpPost("StatusAtualizado_CancelarPedido")]
        public IActionResult CancelarPedido(int id){
            var venda = _vendaService.CancelarPedido(id);
            return Ok(venda);

        }
    }
}