using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace tech_test_payment_api.Models
{
    public enum EnumStatus
    {
        AguardandoPagamento = 1,
        PagamentoAprovado = 2,
        EnviadoParaTransportadora = 3,
        Entregue = 4,
        Cancelada = 5

    }
}