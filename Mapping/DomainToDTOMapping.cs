using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Mapping
{
    public class DomainToDTOMapping : Profile
    {
        public DomainToDTOMapping(){
            CreateMap<Item, ItemDTO>().ReverseMap();
            CreateMap<Vendedor, VendedorDTO>().ReverseMap();
            CreateMap<Venda, VendaDTO>().ReverseMap();
        }
    }
}