using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using tech_test_payment_api.DataBase;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Services
{
    public class VendaService
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        private readonly ItemService _itemService;
        private readonly VendedorService _vendedorService;

        public VendaService(AppDbContext context, IMapper mapper, ItemService itemService, VendedorService vendedorService)
        {
            _context = context;
            _mapper = mapper;
            _itemService = itemService;
            _vendedorService = vendedorService;
        }

        public Venda CriarVenda(VendaDTO vendaDTO, int vendedorId){
            var novaVenda = _mapper.Map<Venda>(vendaDTO);
            var vendedor = _vendedorService.BuscarId(vendedorId);
            novaVenda.Itens = _itemService.AddItens(vendaDTO.Itens);
            novaVenda.Vendedor = vendedor;
            novaVenda.StatusVenda = EnumStatus.AguardandoPagamento;
            _context.Vendas.Add(novaVenda);
            _context.SaveChanges();
            return novaVenda;
        }

        public List<Venda> Listar(){
            return _context.Vendas.ToList();
        }

        public List<Venda> BuscarVendaId(int vendaId){
            return _context.Vendas.Where(x => x.VendaId == vendaId).ToList();
        }
        
        public List<Venda> BuscarVendaStatus(EnumStatus status){
            return _context.Vendas.Where(x => x.StatusVenda == status).ToList();
        }

        public Venda Editar(VendaDTO vendaDTO){
            var venda = _mapper.Map<Venda>(vendaDTO);
            _context.Vendas.Update(venda);
            _context.SaveChanges();
            return venda;
        }

        public Venda Excluir(int id){
            var venda = _context.Vendas.Where(x => x.VendaId == id).FirstOrDefault();
            _context.Vendas.Remove(venda);
            _context.SaveChanges();
            return venda;
        }

        public List<Venda> RealizarPagamento(int id){
            var listaVendas = _context.Vendas.Where(x => x.VendaId == id).ToList();
            if(listaVendas is not null)
                foreach(Venda venda in listaVendas){
                    if(venda.StatusVenda == Models.EnumStatus.AguardandoPagamento)
                        venda.StatusVenda = Models.EnumStatus.PagamentoAprovado;
                        _context.Vendas.Update(venda);
                        _context.SaveChanges();
                }
            return listaVendas;
        }
        public List<Venda> EnviadoParaTransportadora(int id){
            var listaVendas = _context.Vendas.Where(x => x.VendaId == id).ToList();
            if(listaVendas is not null)
                foreach(Venda venda in listaVendas){
                    if(venda.StatusVenda == Models.EnumStatus.PagamentoAprovado)
                        venda.StatusVenda = Models.EnumStatus.EnviadoParaTransportadora;
                        _context.Vendas.Update(venda);
                        _context.SaveChanges();
                }
            return listaVendas;
        }
        public List<Venda> PedidoEntregue(int id){
            var listaVendas = _context.Vendas.Where(x => x.VendaId == id).ToList();
            if(listaVendas is not null)
                foreach(Venda venda in listaVendas){
                    if(venda.StatusVenda == Models.EnumStatus.EnviadoParaTransportadora) 
                       venda.StatusVenda = Models.EnumStatus.Entregue;
                        _context.Vendas.Update(venda);
                        _context.SaveChanges();
                }
            return listaVendas;
        }
        public List<Venda> CancelarPedido(int id){
            var listaVendas = _context.Vendas.Where(x => x.VendaId == id).ToList();
            if(listaVendas is not null)
                foreach(Venda venda in listaVendas){
                    if(venda.StatusVenda == Models.EnumStatus.AguardandoPagamento || venda.StatusVenda == Models.EnumStatus.PagamentoAprovado)
                        venda.StatusVenda = Models.EnumStatus.Cancelada;
                        _context.Vendas.Update(venda);
                        _context.SaveChanges();
                }
            return listaVendas;
        }

    }
}