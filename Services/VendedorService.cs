using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using tech_test_payment_api.DataBase;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Services
{
    public class VendedorService
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;
        public VendedorService(AppDbContext context, IMapper mapper){
            _context = context;
            _mapper = mapper;
        }

        public List<Vendedor> Listar(){
            return _context.Vendedores.ToList();
        }

        public Vendedor BuscarId(int id){
            return _context.Vendedores.Where(x=> x.Id == id).FirstOrDefault();
        }

        public Vendedor Cadastrar(VendedorDTO vendedorDTO){
            var vendedor = _mapper.Map<Vendedor>(vendedorDTO);
            _context.Vendedores.Add(vendedor);
            _context.SaveChanges();
            return vendedor;
        }

        public Vendedor Editar(VendedorDTO vendedorDTO){
            var vendedor = _mapper.Map<Vendedor>(vendedorDTO);
            _context.Vendedores.Update(vendedor);
            _context.SaveChanges();
            return vendedor;
        }

        public Vendedor Excluir(int id){
            var vendedor = _context.Vendedores.Where(x => x.Id == id).FirstOrDefault();
            _context.Vendedores.Remove(vendedor);
            _context.SaveChanges();
            return vendedor;   
        }
    }
}