using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using tech_test_payment_api.DataBase;
using tech_test_payment_api.DTO;
using tech_test_payment_api.Models;

namespace tech_test_payment_api.Services
{
    public class ItemService
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public ItemService(AppDbContext context, IMapper mapper){
            _context = context;
            _mapper = mapper;
        }

        public List<Item> Listar() {
            return _context.Itens.ToList();
        }

        public Item BuscarId(int id) {
            return _context.Itens.Where(x => x.Id == id).FirstOrDefault();
        }

        public Item Cadastrar(ItemDTO itemDTO) {
            var item = _mapper.Map<Item>(itemDTO);
            _context.Itens.Add(item);
            _context.SaveChanges();
            return item;
        }

        public Item Editar(ItemDTO itemDTO) {
            var item = _mapper.Map<Item>(itemDTO);
            _context.Itens.Update(item);
            _context.SaveChanges();
            return item;
        }

        public Item Excluir(int id) {
            var item = _context.Itens.Where(x => x.Id == id).FirstOrDefault();
            _context.Itens.Remove(item);
            _context.SaveChanges();
            return item;
        }

        public List<Item> AddItens(List<ItemDTO> itens) {
            var list = _mapper.Map<List<Item>>(itens);
            foreach(var item in list) {
                _context.Itens.Add(item);
            }
            _context.SaveChanges();
            return list;
        }
    }
}